import os
os.environ["PYSPARK_PYTHON"] = "C:/Users/PC/AppData/Local/Programs/Python/Python310/python.exe"

import logging.config
logging.config.fileConfig('Properties/Configuration/logging.config')
logger= logging.getLogger('Create_pyspark')
from pyspark.sql import SparkSession

def get_spark_object(envn,appName):
    try:
        logger.info('get_spark_object method started')
        if envn =='DEV':
            master='local'
        else:
            master='Yarn'
        logger.info('master is {}'.format(master))
        spark=SparkSession.builder.master(master).appName(appName).enableHiveSupport().config('spark.driver'
                                                                                                '.extraClassPath',
                                                                                                'mysql-connector-java'
                                                                                                '-8.0.29.jar').getOrCreate()
    except Exception as exp:
        logger.error('an error occured in the get_spark_object==',str(exp))
        raise
    else:
        logger.info('Spark object created...')
    return spark